use std::iter::{self, Iterator};
use std::fmt::Debug;
use rand::prelude::*;

const MAX_LEN: usize = 5;
const NUM_ITER: usize = 20_000_000;

fn main() {
    println!("{:>len$} | Avg #flips before it occurs", "Seq", len = MAX_LEN - 1);
    println!("{:->len$}+----------------------------", "", len = MAX_LEN);
    for len in 1..MAX_LEN {
        // the patterns are symmetric
        let first_half = 0..(2u32.pow(len as u32 - 1));
        let second_half = (2u32.pow(len as u32 - 1))..(2u32.pow(len as u32));

        let mut results: Vec<f32> = Vec::with_capacity(first_half.len());
        for x in first_half {
            let pat = into_bitvec(x, len);
            let avg = repeat_and_avg(|| count_until_match(rand_bool_iter(), &pat) as f32, NUM_ITER);
            println!("{:>len$} | {:2.0}", bitvec_to_coinseq(&pat), avg, len = MAX_LEN - 1);
            results.push(avg);
        }

        for (x, avg) in second_half.zip(results.into_iter().rev()) {
            let pat = into_bitvec(x, len);
            println!("{:>len$} | {:2.0}", bitvec_to_coinseq(&pat), avg, len = MAX_LEN - 1);
        }
    }
}

fn rand_bool_iter() -> impl Iterator<Item = bool> {
    let mut rng = thread_rng();
    iter::repeat_with(move || rng.gen())
}

fn count_until_match<T: PartialEq + Debug>(i: impl Iterator<Item = T>, pat: &[T]) -> usize {
    let pat_len = pat.len();
    i.scan(Vec::with_capacity(pat_len + 1), |buf, x| {
            buf.push(x);
            if buf.len() == pat_len + 1 {
                buf.remove(0);
            }
            else if buf.len() > pat_len + 1 {
                panic!();
            }
            Some((buf.len(), buf == pat))
        })
        .skip_while(|&(buf_len, _matches)| buf_len > pat_len)
        .take_while(|&(_buf_len, matches)| !matches)
        .count()
}

fn into_bitvec(x: u32, len: usize) -> Vec<bool> {
    (0..len).map(|i| x & (1 << i) != 0).collect()
}

fn bitvec_to_coinseq(v: &[bool]) -> String {
    v.iter().fold(String::with_capacity(v.len()), |s, &b| s + if b { "H" } else { "T" })
}

fn repeat_and_avg<F: Fn() -> f32>(f: F, n: usize) -> f32 {
    (0..n).fold(0.0, |avg, i| avg + ((f)() - avg) / (i as f32 + 1.0))
}
